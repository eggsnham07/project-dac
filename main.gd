extends Node

var STDOUT = null
var STDIN = null
var version = 0.7

var command_aliases:PoolStringArray = []
var debug = {"commands": false}

signal exit_dac

func create(OUT:TextEdit, IN:LineEdit):
	self.STDOUT = OUT
	self.STDIN = IN
	
	stdout("DAC Version " + String(version))

func do_cmd(input_text:String):
	var command = input_text.split(" ")[0]
	var args = input_text.replace(command + " ", '').split(" ")

	if debug["commands"]:
		print(command)
		print(args.join(", "))
		print(input_text)

	var cmd_file = File.new()

	if command == "exit":
		print("Exiting DAC")
		self.emit_signal("exit_dac")
	elif command == "alias":
		return register_alias(args[1], args[0])

	if cmd_file.file_exists("user://DAC/bin/" + command + ".gd"):
		return load("user://DAC/bin/" + command + ".gd").new().exec(args)
	else:
		for c in command_aliases:
			#print(command_aliases[a])
			if command == c.split(":")[0]:
				return do_cmd(c.split(":")[1] + " " + args.join(" "))
		return("dac: " + command + ": command not found.")

func register_alias(command:String, alias:String):
	command_aliases.append(String(alias + ":" + command))
	print(command_aliases)
	return "Registered new alias"
	
func stdout(text:String):
	text += "\n"
	
	if text == "\n" or text == "echo \n" or text == "echo ": return
	STDOUT.text += text
	if text.to_lower().replace(" ", '') == "\\c":
		STDOUT.text = ""

func download_dac():
	$HTTPRequest.request("https://gitlab.com/eggsnham07/project-dac/-/raw/main/main.gd")